import axios from 'axios'
import { to, action } from '../redux/redux'
import { API, KEY } from '@env'

var err, res
export const find = async coordinate => {
  var { latitude, longitude } = coordinate

  const api = 'https://roads.googleapis.com/v1/nearestRoads?points='
  + latitude + ',' + longitude + '&key=' + KEY;

  [err, res] = await to(axios.get(api))
  const points = res.data['snappedPoints'];
  if(!points || points.length === 0){ return [err, '*no nearby address found*'] }
  const placeId = points[0].placeId

  const api2 = 'https://maps.googleapis.com/maps/api/place/details/json?place_id='
  + placeId + '&key=' + KEY;
  [err, res] = await to(axios.get(api2))
  const address = res.data['result']['formatted_address']

  return [err, address]
}

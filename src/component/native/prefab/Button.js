import React from 'react'
import Native from '../Native'

export default class Button extends Native{

  standard = (text, onPress) =>
    this.touchableOpacity({ text, onPress }, {
      view: { backgroundColor: 'white', borderWidth: 1, borderRadius: 4 },
      text: { fontSize: 15, padding: 5 }
    })

}

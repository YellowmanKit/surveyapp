import React from 'react'
import Native from '../Native'

export default class Text extends Native {

  title = text => this.text({ text }, {
    fontSize: 20, fontWeight: 'bold' })

  part = text => this.text({ text }, {
    fontSize: 15, fontWeight: 'bold', textDecorationLine: 'underline' })

  question = (text, bolds) => this.parsed({ text, bolds }, { fontSize: 15 })

  description = text => this.text({ text }, { fontSize: 10 })

  remark = text => this.text({ text }, { fontSize: 8 })

}

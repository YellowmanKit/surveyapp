import React from 'react'
import Native from '../Native'

export default class Input extends Native {

  choice = (text, onPress, value) => this.checkboxBar({ text, onPress, value }, {
    view: { ...this.size(0.9, 0.025, true), ...this.flex.RowCS },
    checkbox: this.size(0.025, 0.025),
    text: { fontSize: 15 }
  })

  textAnswer = (value, onChangeText, multiline) => this.textArea({ value, onChangeText, multiline }, {
    ...this.size(0.9, 0.05, true),
    fontSize: 15,
    borderWidth: 1
  })

  location = (value, onChangeText) => this.textArea({ value, onChangeText, multiline: true }, {
    ...this.size(0.315, 0.05, true),
    fontSize: 8,
    borderWidth: 1
  })

  option = (value, onPress) => this.checkbox({ value, onPress }, this.size(0.025, 0.025))

}

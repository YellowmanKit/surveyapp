import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'surveyapp/src/component/Component'
import Map from 'surveyapp/src/component/element/Map'

export default class TrafficSignInHongKong extends Component {

  init({ action }){
    ["1A","1B","1C","2A","2B","2C","2D","2E","3A","3B","3C","4A","4B"
    ,"4C","4D","4E","5A","5B","5C","6A","6B","6C","6D","6E","7A","7B","7C",
    "7D","7E","7E2","7_1","7_2","7_3","7_4","7_5","location1","location2",
    "location3","location4","location5","location6","location7","location8",
    "location9","location10","type1A","type1B","type1C","type1D","type2A",
    "type2B","type2C","type2D","type3A","type3B","type3C","type3D","type4A",
    "type4B","type4C","type4D","type5A","type5B","type5C","type5D","type6A",
    "type6B","type6C","type6D","type7A","type7B","type7C","type7D","type8A",
    "type8B","type8C","type8D","type9A","type9B","type9C","type9D","type10A",
    "type10B","type10C","type10D","diss1A","diss1B","diss1C","diss2A",
    "diss2B","diss2C","diss3A","diss3B","diss3C","diss4A","diss4B","diss4C",
    "diss5A","diss5B","diss5C","diss6A","diss6B","diss6C","diss7A","diss7B",
    "diss7C","diss8A","diss8B","diss8C","diss9A","diss9B","diss9C","diss10A",
    "diss10B","diss10C","freq1A","freq1B","freq1C","freq2A",
    "freq2B","freq2C","freq3A","freq3B","freq3C","freq4A","freq4B","freq4C",
    "freq5A","freq5B","freq5C","freq6A","freq6B","freq6C","freq7A","freq7B",
    "freq7C","freq8A","freq8B","freq8C","freq9A","freq9B","freq9C","freq10A",
    "freq10B","freq10C","9A","9B","10A","10B","10C","11A","11B","11C","12A",
    "12B","12C","12D","12E","13A","13B","13C","13D"
    ].map(id => action.survey.init(id, false))
  }

  content = ({ store: { ui: { window, style: { flex } }, survey, map }, action, text, input, button }) =>
    <ScrollView
      contentContainerStyle={{ ...flex.ColSS, padding: '5%' }}>
      {button.standard('back', ()=>action.main.set('status', 'entry'))}
      {this.verGap(0.04)}
      {text.title('Survey of Traffic Signs in Hong Kong')}
      {this.verGap(0.04)}
      {text.part('General Opinions')}
      {this.verGap(0.02)}

      {text.question('1) Regulatory signs are usually circular in shape to give order which may be either mandatory or prohibitory. These include speed limit, stopping restrictions, no entry, and turning restrictions etc. How often do you pay attention to regulatory signs?',
      ['Regulatory signs', 'regulatory signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Whenever I drive', '1A')}
      {this.verGap(0.01)}
      {this.choice('B.   Only when I drive on the roads that I am unfamiliar with', '1B')}
      {this.verGap(0.01)}
      {this.choice('C.   Seldom', '1C')}
      {this.verGap(0.02)}

      {text.question('2) Generally speaking, what is your level of satisfaction with the current regulatory signs in Hong Kong?',
      ['regulatory signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Highly satisfactory', '2A')}
      {this.verGap(0.01)}
      {this.choice('B.   Satisfactory', '2B')}
      {this.verGap(0.01)}
      {this.choice('C.   Average', '2C')}
      {this.verGap(0.01)}
      {this.choice('D.   Unsatisfactory', '2D')}
      {this.verGap(0.01)}
      {this.choice('E.   Highly unsatisfactory', '2E')}
      {this.verGap(0.02)}

      {text.question('3) Warning signs are usually triangular in shape to give warning of hazards ahead. These include warning of merging traffic, sharp bends, roundabout and steep roads etc. How often do you pay attention to warning signs?',
      ['Warning signs', 'warning signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Whenever I drive', '3A')}
      {this.verGap(0.01)}
      {this.choice('B.   Only when I drive on the roads that I am unfamiliar with', '3B')}
      {this.verGap(0.01)}
      {this.choice('C.   Seldom', '3C')}
      {this.verGap(0.02)}

      {text.question('4) Generally speaking, what is your level of satisfaction with the current warning signs in Hong Kong?',
      ['warning signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Highly satisfactory', '4A')}
      {this.verGap(0.01)}
      {this.choice('B.   Satisfactory', '4B')}
      {this.verGap(0.01)}
      {this.choice('C.   Average', '4C')}
      {this.verGap(0.01)}
      {this.choice('D.   Unsatisfactory', '4D')}
      {this.verGap(0.01)}
      {this.choice('E.   Highly unsatisfactory', '4E')}
      {this.verGap(0.02)}

      {text.question('5) Informatory signs normally give road users information or guidance about the route and places. Most informatory signs are rectangular in shape. These include directional signs, get in lance, countdown markers, direction restrictions of each lane in the road ahead etc. How often do you need the help of informatory signs?',
      ['Informatory signs', 'informatory signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Whenever I drive', '5A')}
      {this.verGap(0.01)}
      {this.choice('B.   Only when I drive on the roads that I am unfamiliar with', '5B')}
      {this.verGap(0.01)}
      {this.choice('C.   Seldom', '5C')}
      {this.verGap(0.02)}

      {text.question('6) Generally speaking, what is your level of satisfaction with the current informatory signs in Hong Kong?',
      ['informatory signs'])}
      {this.verGap(0.01)}
      {this.choice('A.   Highly satisfactory', '6A')}
      {this.verGap(0.01)}
      {this.choice('B.   Satisfactory', '6B')}
      {this.verGap(0.01)}
      {this.choice('C.   Average', '6C')}
      {this.verGap(0.01)}
      {this.choice('D.   Unsatisfactory', '6D')}
      {this.verGap(0.01)}
      {this.choice('E.   Highly unsatisfactory', '6E')}
      {this.verGap(0.02)}

      {text.question('If you are dissatisfied with the traffic signs, please answer question 7 and 8 below, otherwise, you may proceed to question 9.')}
      {this.verGap(0.02)}

      {text.question('7) Please tell us the reason(s) why you are dissatisfied with the traffic signs. You may select more than one answers :')}
      {this.verGap(0.01)}
      {this.choice('A.   There are too many signs at a time and you don\'t have enough time to read all of them', '7A')}
      {this.verGap(0.01)}
      {this.choice('B.   There are too few signs on the road and thus not enough information is given', '7B')}
      {this.verGap(0.01)}
      {this.choice('C.   They are not put in the appropriate location so that warning is not given at the right time', '7C')}
      {this.verGap(0.01)}
      {this.choice('D.   The sign itself is not clear enough and cannot convey the exact message to me', '7D')}
      {this.verGap(0.01)}
      {this.choice('E.   Other, please specify:', '7E')}
      {this.verGap(0.01)}
      {this.specify('7E2')}
      {this.verGap(0.02)}

      {text.question('Please also tell us below which traffic signs that you are dissatisfied and reasons.')}
      {this.specify('7_1')}
      {this.verGap(0.01)}
      {this.specify('7_2')}
      {this.verGap(0.01)}
      {this.specify('7_3')}
      {this.verGap(0.01)}
      {this.specify('7_4')}
      {this.verGap(0.01)}
      {this.specify('7_5')}
      {this.verGap(0.02)}

      {text.part('Locations of Unsatisfactory Road Signs')}
      {this.verGap(0.02)}
      {text.question('8) Please name at most 10 locations where you think the traffic signs are unsatisfactory to you :')}
      {this.verGap(0.01)}

      <Div style={{ ...this.size(0.905, 0.66, true), ...flex.RowCS, borderWidth: 2 }}>
        <Div style={{ ...this.size(0.35, 0.65, true), ...flex.ColCS, borderWidth: 1 }}>
          <Div style={{ ...this.size(0.35, 0.15, true), ...flex.ColSS, borderWidth: 1 }}>
            {text.description('Description of the location and reason(s) (eg. Juntion of Road X and Street Y No. 123 of Road Z Road K, near Building J)')}
          </Div>
          {this.location(' 1.','location1')}
          {this.location(' 2.','location2')}
          {this.location(' 3.','location3')}
          {this.location(' 4.','location4')}
          {this.location(' 5.','location5')}
          {this.location(' 6.','location6')}
          {this.location(' 7.','location7')}
          {this.location(' 8.','location8')}
          {this.location(' 9.','location9')}
          {this.location('10.','location10')}
        </Div>
        <Div style={{ ...this.size(0.2, 0.655, true), ...flex.ColCS, borderWidth: 1 }}>
          <Div style={{ ...this.size(0.2, 0.05, true), ...flex.ColSS, borderWidth: 1 }}>
            {text.description('Type of road signs involved')}
          </Div>
          {this.remark('A. Regulatory Signs')}
          {this.remark('B. Warning Signs')}
          {this.remark('C. Informatory Signs')}
          {this.remark('D. Direction Signs')}
          {this.choices([{ id:'type1A', name:'A' },{ id:'type1B', name:'B' },{ id:'type1C', name:'C' },{ id:'type1D', name:'D' }])}
          {this.choices([{ id:'type2A', name:'A' },{ id:'type2B', name:'B' },{ id:'type2C', name:'C' },{ id:'type2D', name:'D' }])}
          {this.choices([{ id:'type3A', name:'A' },{ id:'type3B', name:'B' },{ id:'type3C', name:'C' },{ id:'type3D', name:'D' }])}
          {this.choices([{ id:'type4A', name:'A' },{ id:'type4B', name:'B' },{ id:'type4C', name:'C' },{ id:'type4D', name:'D' }])}
          {this.choices([{ id:'type5A', name:'A' },{ id:'type5B', name:'B' },{ id:'type5C', name:'C' },{ id:'type5D', name:'D' }])}
          {this.choices([{ id:'type6A', name:'A' },{ id:'type6B', name:'B' },{ id:'type6C', name:'C' },{ id:'type6D', name:'D' }])}
          {this.choices([{ id:'type7A', name:'A' },{ id:'type7B', name:'B' },{ id:'type7C', name:'C' },{ id:'type7D', name:'D' }])}
          {this.choices([{ id:'type8A', name:'A' },{ id:'type8B', name:'B' },{ id:'type8C', name:'C' },{ id:'type8D', name:'D' }])}
          {this.choices([{ id:'type9A', name:'A' },{ id:'type9B', name:'B' },{ id:'type9C', name:'C' },{ id:'type9D', name:'D' }])}
          {this.choices([{ id:'type10A', name:'A' },{ id:'type10B', name:'B' },{ id:'type10C', name:'C' },{ id:'type10D', name:'D' }])}
        </Div>
        <Div style={{ ...this.size(0.175, 0.655, true), ...flex.ColCS, borderWidth: 1 }}>
          <Div style={{ ...this.size(0.175, 0.05, true), ...flex.ColSS, borderWidth: 1 }}>
            {text.description('Level of dissatisfaction')}
          </Div>
          {this.remark2('A. Quite dissatisfied')}
          {this.remark2('B. Very dissatisfied')}
          {this.remark2('C. Extremely dissatisfied')}
          {this.choices2([{ id:'diss1A', name:'A' },{ id:'diss1B', name:'B' },{ id:'diss1C', name:'C' }])}
          {this.choices2([{ id:'diss2A', name:'A' },{ id:'diss2B', name:'B' },{ id:'diss2C', name:'C' }])}
          {this.choices2([{ id:'diss3A', name:'A' },{ id:'diss3B', name:'B' },{ id:'diss3C', name:'C' }])}
          {this.choices2([{ id:'diss4A', name:'A' },{ id:'diss4B', name:'B' },{ id:'diss4C', name:'C' }])}
          {this.choices2([{ id:'diss5A', name:'A' },{ id:'diss5B', name:'B' },{ id:'diss5C', name:'C' }])}
          {this.choices2([{ id:'diss6A', name:'A' },{ id:'diss6B', name:'B' },{ id:'diss6C', name:'C' }])}
          {this.choices2([{ id:'diss7A', name:'A' },{ id:'diss7B', name:'B' },{ id:'diss7C', name:'C' }])}
          {this.choices2([{ id:'diss8A', name:'A' },{ id:'diss8B', name:'B' },{ id:'diss8C', name:'C' }])}
          {this.choices2([{ id:'diss9A', name:'A' },{ id:'diss9B', name:'B' },{ id:'diss9C', name:'C' }])}
          {this.choices2([{ id:'diss10A', name:'A' },{ id:'diss10B', name:'B' },{ id:'diss10C', name:'C' }])}
        </Div>
        <Div style={{ ...this.size(0.175, 0.655, true), ...flex.ColCS, borderWidth: 1 }}>
          <Div style={{ ...this.size(0.175, 0.05, true), ...flex.ColSS, borderWidth: 1 }}>
            {text.description('Frequency of driving by the location')}
          </Div>
          {this.remark2('A. Occasionally')}
          {this.remark2('B. About once a week')}
          {this.remark2('C. Nearly everyday')}
          {this.choices2([{ id:'freq1A', name:'A' },{ id:'freq1B', name:'B' },{ id:'freq1C', name:'C' }])}
          {this.choices2([{ id:'freq2A', name:'A' },{ id:'freq2B', name:'B' },{ id:'freq2C', name:'C' }])}
          {this.choices2([{ id:'freq3A', name:'A' },{ id:'freq3B', name:'B' },{ id:'freq3C', name:'C' }])}
          {this.choices2([{ id:'freq4A', name:'A' },{ id:'freq4B', name:'B' },{ id:'freq4C', name:'C' }])}
          {this.choices2([{ id:'freq5A', name:'A' },{ id:'freq5B', name:'B' },{ id:'freq5C', name:'C' }])}
          {this.choices2([{ id:'freq6A', name:'A' },{ id:'freq6B', name:'B' },{ id:'freq6C', name:'C' }])}
          {this.choices2([{ id:'freq7A', name:'A' },{ id:'freq7B', name:'B' },{ id:'freq7C', name:'C' }])}
          {this.choices2([{ id:'freq8A', name:'A' },{ id:'freq8B', name:'B' },{ id:'freq8C', name:'C' }])}
          {this.choices2([{ id:'freq9A', name:'A' },{ id:'freq9B', name:'B' },{ id:'freq9C', name:'C' }])}
          {this.choices2([{ id:'freq10A', name:'A' },{ id:'freq10B', name:'B' },{ id:'freq10C', name:'C' }])}
        </Div>
      </Div>
      {this.verGap(0.02)}
      <Map app={this.app}/>
      {this.verGap(0.02)}
      {button.standard('Reset', ()=>action.map.reset(map))}
      {this.verGap(0.02)}

      {text.part('Personal Profile')}
      {this.verGap(0.02)}

      {text.question('9) You are a :')}
      {this.verGap(0.01)}
      {this.choice('A.   Male', '9A')}
      {this.verGap(0.01)}
      {this.choice('B.   Female', '9B')}
      {this.verGap(0.02)}

      {text.question('10) Which age range do you belong to :')}
      {this.verGap(0.01)}
      {this.choice('A.   Less than 25', '10A')}
      {this.verGap(0.01)}
      {this.choice('B.   25 to 50', '10B')}
      {this.verGap(0.01)}
      {this.choice('C.   More than 50', '10C')}
      {this.verGap(0.02)}

      {text.question('11) How long have you got your driving license?')}
      {this.verGap(0.01)}
      {this.choice('A.   Less than 1 year', '11A')}
      {this.verGap(0.01)}
      {this.choice('B.   Between 1 year to 5 years', '11B')}
      {this.verGap(0.01)}
      {this.choice('C.   More than 5 years', '11C')}
      {this.verGap(0.02)}

      {text.question('12) What type of driver are you?')}
      {this.verGap(0.01)}
      {this.choice('A.   Private Car Driver (Not as a profession)', '12A')}
      {this.verGap(0.01)}
      {this.choice('B.   Taxi Driver', '12B')}
      {this.verGap(0.01)}
      {this.choice('C.   Bus/Mini Bus Driver', '12C')}
      {this.verGap(0.01)}
      {this.choice('D.   Goods Vehicle Driver', '12D')}
      {this.verGap(0.01)}
      {this.choice('E.   Other Type of Professional Driver', '12E')}
      {this.verGap(0.02)}

      {text.question('13) How much time do you usually spend on driving weekly?')}
      {this.verGap(0.01)}
      {this.choice('A.   More than 40 hours', '13A')}
      {this.verGap(0.01)}
      {this.choice('B.   >20 ~ 40 hours', '13B')}
      {this.verGap(0.01)}
      {this.choice('C.   >10 ~ 20 hours', '13C')}
      {this.verGap(0.01)}
      {this.choice('D.   Less than or equal to 10 hours', '13D')}
      {this.verGap(0.02)}

      {button.standard('Submit', ()=>action.survey.submit(1, survey))}
      {this.verGap(0.02)}

      {text.part('- Thank you for your cooperation -')}
    </ScrollView>

  choice = (text, id,) =>
    this.input.choice(text, ()=>this.action.survey.set(id, !this.store.survey[id]), this.store.survey[id])

  specify = id =>
    this.input.textAnswer(this.store.survey[id], text=>this.action.survey.set(id, text), false)

  remark = (text, style) =>
    <Div style={{ ...this.size(0.2, 0.025, true), ...this.flex.ColSS, borderWidth: 1 }}>
      {this.text.remark(text)}
    </Div>

  remark2 = text =>
    <Div style={{ ...this.size(0.175, 0.033, true), ...this.flex.ColSS, borderWidth: 1 }}>
      {this.text.remark(text)}
    </Div>

  location = (name, id) =>
    <Div style={{ ...this.size(0.35, 0.05, true), ...this.flex.RowSS, borderWidth: 1 }}>
      {this.text.remark(name)}
      {this.input.location(this.store.survey[id], text=>this.action.survey.set(id, text), true)}
    </Div>

  choices = options =>
    <Div style={{ ...this.size(0.2, 0.05, true), ...this.flex.RowSS, borderWidth: 1 }}>
      {options.map(option => this.option(option))}
    </Div>

  choices2 = options =>
    <Div style={{ ...this.size(0.15, 0.05, true), ...this.flex.RowSS, borderWidth: 1 }}>
      {options.map(option => this.option(option))}
    </Div>

  option = ({ id, name }) =>
    <Div key={id} style={{ ...this.size(0.05, 0.05, true), ...this.flex.ColCC, borderWidth: 1 }}>
      {this.text.remark(name)}
      {this.input.option(this.store.survey[id], ()=>this.action.survey.set(id, !this.store.survey[id]))}
    </Div>
}

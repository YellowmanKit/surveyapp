import React from 'react'
import { SafeAreaView as Div } from 'react-native'
import Component from 'surveyapp/src/component/Component'

import TrafficSignInHongKong from './views/TrafficSignInHongKong'

export default class Survey extends Component {

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      <TrafficSignInHongKong app={this.app}/>
    </Div>

}

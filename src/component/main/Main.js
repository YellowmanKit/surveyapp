import React from 'react'
import Component from 'surveyapp/src/component/Component'
import { SafeAreaView as Div, Dimensions, Text } from 'react-native'
import Loading from './mask/Loading'
import Entry from './entry/Entry'
import Survey from './survey/Survey'
import Done from './done/Done'

export default class Main extends Component {

  init({ action }){
    action.main.set('status', 'entry')
    action.ui.set('window', Dimensions.get('window') )
  }

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {this.page(this.app)}
      <Loading app={this.app}/>
    </Div>

  page = ({ store: { main: { status } } }) => {
    switch (status) {
      case 'entry':
        return <Entry app={this.app}/>
      case 'survey':
        return <Survey app={this.app}/>
      case 'done':
        return <Done app={this.app}/>
      default:
        return null
    }
  }

}

import React from 'react'
import { SafeAreaView as Div } from 'react-native'
import Component from 'surveyapp/src/component/Component'

export default class Entry extends Component {

  content = ({ store: { ui: { window, style: { flex } } }, action, button }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {button.standard('Survey', ()=>action.main.set('status', 'survey'))}
    </Div>

}

import React from 'react'
import { SafeAreaView as Div } from 'react-native'
import Component from 'surveyapp/src/component/Component'

export default class Done extends Component {

  content = ({ store: { ui: { window, style: { flex } } }, action, text, button }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {text.question('Submitted successfully, thank you!')}
      {this.verGap(0.02)}
      {button.standard('Back', ()=>action.main.set('status', 'entry'))}
    </Div>

}

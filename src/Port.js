import React from 'react'
import Main from 'surveyapp/src/component/main/Main'

import Func from './utility/Func'
import Input from './component/native/prefab/Input'
import Button from './component/native/prefab/Button'
import Text from './component/native/prefab/Text'

class Port extends React.Component {

  render = () => this.main(this.props)

  main({ store, action }){
    const props = { app: { store, action, func: new Func({ app: { store }}) } }
    return(
      <Main app={{
        ...this.state, ...props.app,
        text: new Text(props),
        input: new Input(props),
        button: new Button(props)
      }}/>
    )
  }

}

import actions from './redux/actions'
import { connect } from 'react-redux'
export default connect(store => { return { store } }, dispatch => { return actions(dispatch) })(Port)

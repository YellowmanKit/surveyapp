import axios from 'axios'
import { to, action } from '../../redux'
import { API } from '@env'

var err, res
export const submit = (code, survey) => {
  return async dispatch => {
    [err, res] = await to(axios.post(API + 'submit', {
      "survey": code,
      "values": Object.keys(survey).map(key => [key, survey[key]])
    }))
    dispatch(action.set('status', 'done'))
  }
}

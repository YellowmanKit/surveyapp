import { bindActionCreators } from 'redux'
import * as main from './control/main/action'
import * as map from './control/map/action'
import * as ui from './control/ui/action'
import * as survey from './data/survey/action'

const add = (action, dispatch) => {
  action.init = (key, value) => { return { type: 'init', payload: { key, value } } }
  action.set = (key, value) => { return { type: 'set', payload: { key, value } } }
  return bindActionCreators(action, dispatch)
}

export default actions = dispatch => { return { action: {
  main: add(main, dispatch),
  map: add(map, dispatch),
  ui: add(ui, dispatch),

  survey: add(survey, dispatch)
}}}

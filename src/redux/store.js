import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { main } from './control/main/reducer'
import { map } from './control/map/reducer'
import { ui } from './control/ui/reducer'

import { survey } from './data/survey/reducer'

const reducer = combineReducers({
  main, map, ui,
  survey
})

export default createStore(reducer, {}, applyMiddleware(thunk))

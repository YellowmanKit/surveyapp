import { reducer } from '../../redux'

export const ui = (
  state = {
    style: { flex },
    window: { width: 0, height: 0 }
  }, action) => reducer(state, action, result)

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}

const flex = {
  ColCC: { flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
  ColCS: { flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' },
  ColCE: { flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-end' },
  ColSS: { flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start' },
  ColEE: { flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'flex-end' },

  RowCC: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
  RowSS: { flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start' },
  RowCS: { flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }
}

import axios from 'axios'
import { to, action } from '../../redux'
import { API, KEY } from '@env'
import { find } from '../../../utility/Address'

var err, res

export const reset = ({ markers }) => {
  return async dispatch => {
    for(var count = 1;count <= 10;count++){
      if(markers[count - 1]){
        markers[count - 1] = false
        dispatch(action.set('location' + count, ''))
        dispatch(action.set('markers', markers))
      }
    }
  }
}

export const onRemove = ({ markers }, count ) => {
  return async dispatch => {
    if(markers[count - 1]){
      markers[count - 1] = false
      dispatch(action.set('location' + count, ''))
      dispatch(action.set('markers', markers))
    }
  }
}

export const onPress = ({ markers }, coordinate) => {
  return async dispatch => {
    dispatch(action.set('loading', true));

    var count = 0
    if(markers.length > 9){ return }
    for(var i=0;i<markers.length;i++){
      if(!markers[i]){
        count = i + 1
        markers[i] = { count, coordinate }
        break
      }
      if(i === markers.length - 1){
        count = i + 2
        markers.push({ count, coordinate })
        break
      }
    }
    if(markers.length === 0){
      count = 1
      markers.push({ count, coordinate })
    };
    var { latitude, longitude } = coordinate;
    [err, res] = await find(coordinate)
    const address = res

    dispatch(action.set('loading', false));
    dispatch(action.set('location' + count, address + '(' + latitude + ',' + longitude + ')'))
    dispatch(action.set('markers', markers))
  }
}
